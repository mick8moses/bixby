# mick8moses/dotnet1.1.1-mongo3.4-apacheutils
# Dockerizing .NET Core + MongoDB + ApacheBench + ncftp:
# Dockerfile for continuous integration testing of .NET CORE Application with MongoDB Database in Bitbucket Pipeline

# get microsoft dotnet core on Debian image
FROM microsoft/dotnet:1.1.1-sdk
MAINTAINER mick8moses <mick8moses@yahoo.com>

# Import MongoDB public GPG key AND create a MongoDB list file
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
RUN echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list

# Update apt-get sources AND install MongoDB
# update apt's internal database of available packages
# & installation of ApacheBench for Perf testing
# & installation of MongoDB
RUN apt-get -y update && apt-get install -y apache2-utils && apt-get install -y mongodb-org && apt-get install -y ncftp

# Create the MongoDB data directory
RUN mkdir -p /data/db

# Expose port #27017 from the container to the host
EXPOSE 27017

# Set /usr/bin/mongod as the dockerized entry-point application
ENTRYPOINT ["/usr/bin/mongod"]