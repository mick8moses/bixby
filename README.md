# BIXBY #

[Latest Azure API Release Candidate Deployment - www.feralfingers.com](http://www.feralfingers.com)

[Latest Drone Markdown Documentation - pushed from Dev Build](http://13.82.226.103/)

[Docker Hub Image for Continuous Integration Builds](https://hub.docker.com/r/mick8moses/dotnet2-mongo3.4-apacheutils/)

[Docker Cloud Release Candidate Images ](https://cloud.docker.com/swarm/mick8moses/repository/docker/mick8moses/bixby-rc-build/general)

Bixby is a reference solution used as an instructional tool for budding software engineers.
It's motive is to exhibit a DevOps Pipeline of the Continuous Continuum (Build, Integration, Test, Delivery, Deployment, Documentation).
Along with, best practices and proper scaffolding for an operational software solution.

### Solution Components ###
* **.NET Core [C#]** = RESTful API + Business Logic + Data Access (currently v2.0)
* **MongoDB [json]** = Persistance - NoSQL Database (currently v3.4)
* **Swashbuckle.AspNetCore[C#]** = REST API interactive ui console from HTTP Markup & Controller Comments
* **MSTest [C#]** = Testing framework (automated on continuous integration builds)
* **ApacheBench** = Simple Load Testing framework (automated on continuous integration builds)
* **BDDfy [C#]** = Behavior Driven Design Framework for Detailing supported User Stories & Scenarios sitting on testing framework (also documentation creation)
* **Bitbucket [Git]** = Source Control Code Repository
* **Bitbucket Pipelines [yml/bash]** = Build Orchestrator for Continuous Build & Test (development branch), Continuous Package & Delivery (master branch)
* **Debian [Linux]** = Base Operating System in Docker containers
* **Docker** = Software Container Platform (Utilized for Development, Continuous Integration, & Release Candidate Build)
* **Docker Cloud** = Docker image registry & repository
* **Docker Swarm + Compose** = Swarm assigns resources in the cloud and Compose defines the docker images and the network and deploys to the swarm resources.
* **Drone.io** = Continous Delivery platform for pushing Docker stack to the bixby swarm hosted in Azure (hosted in azure) NOT CURRENTLY OPERATIONAL
* **Azure** = Cloud Hosting for Release Candidate swarm.

### How do I start MongoDB on my development environment & run the test suite via Visual Studio? ###

Step One: Install MongoDB Community edition for your operating system locally and create a folder to hold Mongo Databases (e.g. c:\MongoDBs)

Open command prompt and navigate to the mongod.exe folder (e.g. "C:\Program Files\MongoDB\Server\3.4\bin") and run:
```
$ mongod --dbpath C:\MongoDBs --port 27017
```

This will start your MongoDB server listening on port 27017 (i.e. mongodb://localhost:27017)
Currently when running feature tests locally and through the continuous integration build it looks for the local install of MongoDB

Then you can run all tests in Visual Studio 2017

### How do I develop locally using Visual Studio 2017 + Docker on Windows 10? ###

* Install Docker (this will require turning on Hyper-v) and in the settings share your "c" drive
* Install Visual Studio 2017 & set docker-compose as startup project

Now when you start debugging via VS 2017 it will insert your code into docker images for debugging
You should see swagger at http://localhost/swagger 
& the sample soap service at http://localhost/SoapService.svc?wsdl (Wizdler is a nice chrome plugin to play with it)

To use the Weights Controller it is secured by a JWT token that you can retrieve by a "POST" to the Token Controller,
copy the string value of the returned token, click the "authorize" button on the top bar of Swagger and for the api key
enter "Bearer token-value", then press authorize.

You may want to prune old containers and images
```
$ docker stop $(docker ps -a -q)
$ docker rm -f $(docker ps -a -q)
$ docker rmi -f $(docker images -a -q)
```

### How do I run the source locally via command line? ###

Open command prompt and navigate to the src/Bixby.Web folder and run:
```
$ dotnet run
```

This will start your web site running and it should say what port it's running on: "Now listening on: http://localhost:5000"

### How do I download the latest RC build and run it locally (e.g. on a Mac) using Docker? ###

Make sure docker is installed locally

Open Terminal Session
First you may want to stop & remove all docker containers & images with:
```
$ docker stop $(docker ps -a -q)
$ docker rm -f $(docker ps -a -q)
$ docker rmi -f $(docker images -a -q)
```

Let's start the swarm and deploy an RC build locally (use development computer as node)
```
$ docker swarm init
$ docker stack deploy -c bixby-rc-docker-compose-stack.yml bixbystack
```

View the stack
```
$ docker stack ps bixbystack
```

Pull up swagger ui in browser with:
[http://localhost:8080/swagger/](http://localhost:8080/swagger/)

Tear down the stack when done
```
$ docker stack rm bixbystack
$ docker swarm leave -f
$ docker stack ls
```

To deploy locally as a swarm cluster (on multiple machines like deployment in the cloud)
Install Virtualbox locally

Create a pair of VMs locally using the Virtualbox driver
```
$ docker-machine create --driver virtualbox myvm1
$ docker-machine create --driver virtualbox myvm2
$ docker-machine ls
```

Make the first VM a swarm manager and initialize the swarm (use IP and port from above)
```
$ docker-machine ssh myvm1 "docker swarm init --advertise-addr 192.168.99.100:2376"
```

Have the second machine join the swarm (use the command from above but always use 277 as the port)
```
$ docker-machine ssh myvm2 "docker swarm join --token SWMTKN-1-41v2iwa4wu4ldcvyfd61xkxn3gnn09mxuvhxvdnlqe8ia8hqh8-csi1otb03crzwiuk1fn7407o4 192.168.99.100:2377"
```

Check the swarm by ssh into the leader/ma
```
$ docker-machine ssh myvm1 "docker node ls"
```

Create the data directory on the manager for storage
```
$ docker-machine ssh myvm1 "mkdir ./data"
```

Copy the rc docker compose yml file to the leader node
```
$ docker-machine scp bixby-rc-docker-compose-stack.yml myvm1:~
```

Deploy the stack on the cluster
```
$ docker-machine ssh myvm1 "docker stack deploy -c bixby-rc-docker-compose-stack.yml bixbystack"
```

Verify the stack on the cluster
```
$ docker-machine ssh myvm1 "docker stack ps bixbystack"
```

Pull up docker visualizer ui in browser using manager vm IP:
[http://192.168.99.100:8090/](http://192.168.99.100:8090/)

Pull up swagger ui in browser using either IP:
[http://192.168.99.101:8080/swagger/](http://192.168.99.101:8080/swagger/)

Tear down the stack on the cluster
```
$ docker-machine ssh myvm1 "docker stack rm bixbystack"
```

Remove the swarm
```
$ docker-machine ssh myvm2 "docker swarm leave"
$ docker-machine ssh myvm1 "docker swarm leave --force"
```

Stop & remove existing VMs
```
$ docker-machine stop $(docker-machine ls -q)
$ docker-machine rm $(docker-machine ls -q)
```

### How do I push to the azure cloud? ###

Attach to the docker swarm (swarm_bixby) using docker
Update bixby-rc-docker-compose-stack.yml with the tag for the RC build you want to deploy

Check nodes in docker swarm in azure
```
$ docker node ls
```

Navigate to directory with the compose file

Deploy stack to Docker swarm on Azure
```
$ docker stack deploy -c bixby-rc-docker-compose-stack.yml bixbystack
```

Check stack in swarm
```
$ docker stack services bixbystack
```

Tear down stack in swarm
```
$ docker stack rm bixbystack
```

### Contribution guidelines (coming soon) ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

##### Michael Miller #####
##### mick@thunderboltisland.com #####
