﻿using Bixby.Web.Controllers;
using Bixby.Web.Data;
using Bixby.Web.Model;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestStack.BDDfy;

namespace Bixby.Web.Test.Controllers
{
    [TestClass]
    [Story(Title = "Weights Controller",
            AsA = "As someone trying to lose weight",
            IWant = "I want to record my weight measurement",
            SoThat = "So that I may track progress toward my goal")]
    public class WeightsControllerTests : TestBase
    {
        #region Private variables
        private static Settings _dbSettings = new Settings(){ConnectionString= "mongodb://localhost:27017", Database= "WeightsDB_AutoTests" };
        private static MongoClient _mongoClient;
        private string _weightId;
        private string _weight;
        private string _measuredDateTime;
        private WeightsController _weightsController = new WeightsController(new WeightRepo( Options.Create(_dbSettings)));
        private Task<WeightInfo> _response;
        private List<WeightInfo> _responseGetAll;
        // Holding List of added measurement ids for Integration Tests
        static List<WeightInfo> myMeasurements = new List<WeightInfo>();
        #endregion

        #region Integration Test Class Setup & Teardown
        [ClassInitialize]
        public static void SetupEnvironment(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext context)
        {
            // drop database if it's there from previous testing
            _mongoClient = new MongoClient(_dbSettings.ConnectionString);
            if (_mongoClient != null)
                 _mongoClient.DropDatabase(_dbSettings.Database);
        }
        #endregion

        #region Scenarios
        // These are ordered integration tests
        [TestMethod]
        public void Scenario0001AddNewWeightMeasurement()
        {
            string weightId = default(string);
            string weight = default(string);
            string measuredDateTime = default(string);

            Random rnd = new Random();

            Console.Write("Scenario001AddNewWeightMeasurement");

            this.Given(s => s.GivenNewWeights(weightId, weight, measuredDateTime), "Given new weights listed in the example table")
                .When(s => s.WhenEachWeightIsPostedToTheWeightsController())
                .Then(s => s.ThenItWillRespondWithTheNewWeightSaved(), "Then it will respond with the status code listed in the example table")
                .WithExamples(new ExampleTable("weightId", "weight", "measuredDateTime")
                                {
                                    { Guid.NewGuid().ToString(), rnd.Next(1,999), DateTimeOffset.Now.ToString()},   // Success: Valid Weight Provided
                                    { "",  rnd.Next(1,999), DateTimeOffset.Now.ToString()},                         // Success: Valid Weight Provided
                                    { Guid.NewGuid().ToString(),  rnd.Next(1,999), ""},                             // Success: Valid Weight Provided
                                    { "",  rnd.Next(1,999), ""},                                                    // Success: Valid Weight Provided
                                    { "", "", ""}                                                                   // Failure: No Weight Provided
                                })
                .BDDfy();
        }
        [TestMethod]
        public void Scenario0002GetAllWeightMeasurements()
        {
            this.Given(s => s.GivenThereAreWeightMeasurementsStored())
                .When(s => s.WhenTheGetAllMethodOfTheWeightsControllerIsCalled())
                .Then(s => s.ThenItWillReturnAllMeasurements())
                .BDDfy();
        }
        #endregion

        #region Private Methods
        private void GivenNewWeights(string weightId, string weight, string measuredDateTime)
        {
            _weightId = weightId;
            _weight = weight;
            _measuredDateTime = measuredDateTime;
        }

        private void WhenEachWeightIsPostedToTheWeightsController()
        {
            // convert to DateTimeOffset from string
            DateTimeOffset measuredDateTime;
            DateTimeOffset.TryParse(_measuredDateTime, out measuredDateTime);
            //converst to float from string
            float weight;
            float.TryParse(_weight, out weight);

            _response = _weightsController.Post(new WeightInfo()
            {
                Id = _weightId,
                Weight = weight,
                MeasuredDateTime = measuredDateTime
            });
        }

        private void ThenItWillRespondWithTheNewWeightSaved()
        {
            // a weight was saved
            if (_weight.Length > 0)
            {
                Assert.AreEqual(_weight, _response.Result.Weight.ToString());
                Assert.IsFalse(string.IsNullOrEmpty(_response.Result.Id));
                Assert.IsFalse(string.IsNullOrEmpty(_response.Result.MeasuredDateTime.ToString()));
                // Hold newly formed Measurements for future integration tests
                myMeasurements.Add(_response.Result);
            }
            // no weight was saved
            else
                Assert.IsNull(_response.Result);
        }

        private void GivenThereAreWeightMeasurementsStored()
        { }

        private void WhenTheGetAllMethodOfTheWeightsControllerIsCalled()
        {
            _responseGetAll = _weightsController.Get().Result.ToList();
        }

        private void ThenItWillReturnAllMeasurements()
        {
            // compare the lists
            AreEqualByJson(myMeasurements, _responseGetAll);
        }

        /// <summary>
        /// Compare two objects by property values
        /// </summary>
        /// <param name="expected">expected object</param>
        /// <param name="actual">actual object</param>
        public static void AreEqualByJson(object expected, object actual)
        {
            var a = Object.Equals(expected, actual);
            var b = expected.Equals(actual);
            var expectedJson = JsonConvert.SerializeObject(expected);
            var actualJson = JsonConvert.SerializeObject(actual);
            var c = Object.Equals(expectedJson, actualJson);
            var d = expectedJson.Equals(actualJson);
            Assert.IsTrue(expectedJson.Equals(actualJson));
        }
        #endregion
    }
}
