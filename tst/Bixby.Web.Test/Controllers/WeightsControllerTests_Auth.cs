﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TestStack.BDDfy;

namespace Bixby.Web.Test.Controllers
{
    [TestClass]
    [Story(Title = "Weights Controller Authorization",
        AsA = "As someone recording measurements",
        IWant = "I want only myself to access my records",
        SoThat = "So that others can't see them")]
    public class WeightsControllerTests_Auth : TestBase
    {
        #region Private variables
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private string _username;
        private string _password;

        private HttpResponseMessage _response;
        #endregion

        // On construction of the test class set up a server
        public WeightsControllerTests_Auth()
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Path.GetFullPath(@"../../.."))
            .AddJsonFile("appsettings.json", optional: false)
            //.AddUserSecrets<Startup>()
            .Build();

            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .UseConfiguration(configuration));
            _client = _server.CreateClient();
        }


        #region Scenarios
        // These are ordered integration tests
        [TestMethod]
        public void Scenario1001AuthorizationToWeightsController()
        {
            string username = default(string);
            string password = default(string);
            string statusCode = default(string);

            Console.Write("Scenario1001AuthorizationToWeightsController");

            this.Given(s => s.GivenCredentials(username, password))
                .When(s => s.WhenThoseCredentialsArePostedToTheWeightsController())
                .Then(s => s.ThenItWillAuthorizeAppropriately(statusCode), "Then it will be authorized with valid credentials")
                .WithExamples(new ExampleTable("username", "password", "statusCode")
                                {
                                    { Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), "OK"},      // Authorized - username & password
                                    { "",  Guid.NewGuid().ToString(), "Unauthorized"},                          // Not Authorized - no username
                                    { Guid.NewGuid().ToString(),  "", "Unauthorized"}                           // Not Authorized - no password
                                })
                .BDDfy();
        }
        #endregion

        #region Private Methods
        private void GivenCredentials(string username, string password)
        {
            _username = username;
            _password = password;
        }
        private async Task WhenThoseCredentialsArePostedToTheWeightsController()
        {
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, "/api/weights");

            string loginString = @"{username: """ + _username + @""", password: """ + _password + @"""}";
            HttpResponseMessage responseTokenController = await _client.PostAsync("/api/token", new StringContent(loginString, Encoding.UTF8, "application/json"));
            string tokenString = await responseTokenController.Content.ReadAsStringAsync();

            if (!string.IsNullOrEmpty(tokenString))
            {
                JObject tokenJson = JObject.Parse(tokenString);
                string token = (string)tokenJson["token"];
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            _response = await _client.SendAsync(requestMessage);

            //    Assert.AreEqual(HttpStatusCode.OK, booksResponse.StatusCode);

            //    var bookResponseString = await booksResponse.Content.ReadAsStringAsync();
            //    var bookResponseJson = JArray.Parse(bookResponseString);
            //    Assert.AreEqual(true, bookResponseJson.Count == 4);
            //_response = await _client.GetAsync("/api/weights");
        }

        private void ThenItWillAuthorizeAppropriately(string statusCode)
        {
            Assert.AreEqual(statusCode, _response.StatusCode.ToString());
        }
        #endregion

    }
}