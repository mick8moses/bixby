﻿using Bixby.Web.Controllers;
using Bixby.Web.Data;
using Bixby.Web.Model;
using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TestStack.BDDfy;
using Hl7.Fhir.Serialization;
using Hl7.Fhir.ElementModel;


namespace Bixby.Web.Test.Controllers
{
    [TestClass]
    [Story(Title = "FHIR Controller",
            AsA = "As a healthcare provider",
            IWant = "I want to store and retrieve clinical patient information",
            SoThat = "So that I may provide informed patient care")]
    public class FhirControllerTests : TestBase
    {
        #region Private variables
        private static Settings _dbSettings = new Settings() { ConnectionString = "mongodb://localhost:27017", Database = "FhirDB_AutoTests" };
        private static MongoClient _mongoClient;
        private FhirController _fhirController = new FhirController(new FhirRepo(Options.Create(_dbSettings)));
        private IActionResult _response;
        private string _fhirResourceType;
        private string _fhirResource;

        #endregion

        #region Integration Test Class Setup & Teardown
        [ClassInitialize]
        public static void SetupEnvironment(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext context)
        {
            // drop database if it's there from previous testing
            _mongoClient = new MongoClient(_dbSettings.ConnectionString);
            if (_mongoClient != null)
                _mongoClient.DropDatabase(_dbSettings.Database);
        }
        #endregion

        #region Scenarios
        // These are ordered integration tests
        [TestMethod]
        public void Scenario9001AddNewFhirResource()
        {
            string fhirResourceType = default(string);
            string fhirResource = default(string);
            string statusCode = default(string);

            Console.Write("Scenario9001AddNewFhirResources");

            this.Given(s => s.GivenNewFhirResources(fhirResourceType, fhirResource), "Given new FHIR resources listed in the example table")
                .When(s => s.WhenEachResourceIsPostedToTheFhirController())
                .Then(s => s.ThenItWillRespondWithTheAppropriateStatusCodeAndBody(statusCode), "Then it will respond with the status code and the created resource listed in the example table ")
                .WithExamples(new ExampleTable("fhirResourceType", "fhirResource", "statusCode")
                                {
                                    { "Patient",  "", "400"},                                   // Bad Request: No FHIR Resource Provided 
                                    //{ "",  "TestData/patient003.json", "400"}                 // Bad Request: No FHIR Resource Type Provided 
                                    //{ "NotARealResource", "TestData/patient003.json", "422"}    // Bad Request: FHIR Resource can't be created
                                    //{ "Patient", "TestData/patient003.json", "201"}             // Created: FHIR Resource Created
                                })
                .BDDfy();
        }
        #endregion

        #region Private Methods
        private void GivenNewFhirResources(string fhirResourceType, string fhirResource)
        {
            _fhirResourceType = fhirResourceType;
            _fhirResource = fhirResource;
        }

        private async System.Threading.Tasks.Task WhenEachResourceIsPostedToTheFhirController()
        {
            //if (_fhirResourceType.ToLower() == "patient")
            //{
                // FHIR Patient Object
                Patient patient = null;

                if (_fhirResource.Length > 0)
                {
                    string stringPatient = (_fhirResource.Length > 0) ? File.ReadAllText(_fhirResource) : "";

                    patient = new FhirJsonParser().Parse<Patient>(stringPatient);
                }

                _response = await _fhirController.Post(_fhirResourceType, patient);
            //}


            //var json = TestDataHelper.ReadTestData("TestPatient.json");

            //var pocoP = new PocoNavigator((new FhirJsonParser()).Parse<Patient>(json));
            //var jsonP = JsonDomFhirNavigator.Create(json);

            //var compare = pocoP.IsEqualTo(jsonP);

            //if (compare.Success == false)
            //{
            //    Debug.WriteLine($"Difference in {compare.Details} at {compare.FailureLocation}");
            //    Assert.Fail();
            //}


            //string stringResource = File.ReadAllText(_fhirResource);

            //IElementNavigator patient = JsonDomFhirNavigator.Create(stringResource);

            //patient.MoveToFirstChild("gender");
            //Assert.AreEqual("male", patient.Value.ToString());
            //Assert.AreEqual("Patient.gender[0]", patient.Location);



            //Patient patient = JsonConvert.DeserializeObject<Patient>(File.ReadAllText(_fhirResource));

            //_response = await _fhirController.Post(_fhirResourceType, patient);
        }

        private void ThenItWillRespondWithTheAppropriateStatusCodeAndBody(string statusCode)
        {
            //if(_response.GetType() == typeof(ObjectResult))
                Assert.AreEqual(statusCode, ((ObjectResult)_response).StatusCode.ToString());
            // check the status codes is correct
           // Assert.AreEqual(statusCode, _response.StatusCode.ToString());
        }

        #endregion
    }
}


