﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.BDDfy.Configuration;

namespace Bixby.Web.Test
{
    [TestClass]
    public abstract class TestBase
    {

        [TestInitialize]
        public void Setup()
        {
            // Turned off for now - seems to have an error
            Configurator.BatchProcessors.MarkDownReport.Enable();
            Configurator.BatchProcessors.DiagnosticsReport.Enable();
            Configurator.BatchProcessors.HtmlReport.Enable();
            //Configurator.BatchProcessors.HtmlMetroReport.Enable();
            //Configurator.BatchProcessors.Add(new HtmlReporter(new CustomMetroHtmlReportConfiguration(), new MetroReportBuilder()));
        }

    }
}