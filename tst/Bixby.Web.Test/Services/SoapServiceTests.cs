﻿using Bixby.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using TestStack.BDDfy;


namespace Bixby.Web.Test.Controllers
{
    [TestClass]
    [Story(Title = "Soap Service",
            AsA = "As someone trying to send a SOAP message",
            IWant = "I want to have my message bounced back to me in reverse",
            SoThat = "So that I know I can send a SOAP message")]
    public class SoapServiceTests : TestBase
    {
        #region Private variables
        private string _soapMsg;
        private string _response;
        private SoapService _soapController = new SoapService();
        #endregion

        #region Scenarios
        // These are ordered integration tests
        [TestMethod]
        public void Scenario2001SendSoapMessageToTheService()
        {
            string soapMsg = "RegaL KnitS "+ Guid.NewGuid().ToString();

            Random rnd = new Random();

            Console.Write("Scenario0001SendSoapMessageToTheService");

            this.Given(s => s.GivenNewSoapMessage(soapMsg), "Given a new SOAP message")
                .When(s => s.WhenTheMessageIsSentToTheSoapController())
                .Then(s => s.ThenItWillRespondWithTheMessageInReverse())
                .BDDfy();
        }
        #endregion

        #region Private Methods
        private void GivenNewSoapMessage(string soapMsg)
        {
            _soapMsg = soapMsg;
        }

        private void WhenTheMessageIsSentToTheSoapController()
        {
            _response = _soapController.Ping(_soapMsg);
        }

        private void ThenItWillRespondWithTheMessageInReverse()
        {
            Assert.AreEqual(string.Join(string.Empty, _soapMsg.Reverse()), _response);
        }
        #endregion
    }
}
