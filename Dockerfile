# This Dockerfile is used for creating release candidate image of Bixby.Web on Docker
# Build docker image from the parent image: microsoft apnetcore version 2.0 image
FROM microsoft/aspnetcore:2.0

# Set the working directory to /app
WORKDIR /app

# Copy our code from the publish folder (navigating from this Dockerfile) to the working folder of the container
COPY ./src/Bixby.Web/bin/Release/netcoreapp2.0/publish .

# Define an environment variable for the Web API traffic
ENV ASPNETCORE_URLS http://+:80
# Expose port 80 to the world outside this container
EXPOSE 80

# When the container starts execute the dotnet command on the Bixby.Web.dll
ENTRYPOINT ["dotnet", "Bixby.Web.dll"]