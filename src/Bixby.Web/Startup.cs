﻿
using Bixby.Web.Data;
using Bixby.Web.Interfaces;
using Bixby.Web.Model;
using Bixby.Web.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using SoapCore;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using Bixby.Web.Infrastructure;

namespace Bixby.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add JWT authentication schema
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                      .AddJwtBearer(options =>
                      {
                          options.TokenValidationParameters = new TokenValidationParameters
                          {
                              ValidateIssuer = true,                            //validate the server that created the token
                              ValidateAudience = true,                          //ensure that the recipient of the token is authorized to receive it 
                              ValidateLifetime = true,                          //check that the token is not expired and that the signing key of the issuer is valid
                              ValidateIssuerSigningKey = true,                  //verify that the key used to sign the incoming token is part of a list of trusted keys 
                              ValidIssuer = Configuration["Jwt:Issuer"],
                              ValidAudience = Configuration["Jwt:Issuer"],
                              IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                          };
                      });

            // Add SOAP Services
            services.TryAddSingleton<SoapService>();
            services.TryAddSingleton<XcpdService>();

            services.AddSoapExceptionTransformer((ex) => ex.Message);

            services.AddMvc();

            // Add Swagger for UI for API
            services.AddSwaggerDocumentation();

            // Add MongoDB 
            services.Configure<Settings>(options =>
            {
                options.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database = Configuration.GetSection("MongoConnection:Database").Value;
            });

            Console.WriteLine("My Connection String: " + Configuration.GetSection("MongoConnection:ConnectionString").Value);

            services.AddTransient<IWeight, WeightRepo>();
            services.AddTransient<IFhir, FhirRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // debugging statement to see environment i'm in
            Console.WriteLine("My Environment: " + env.EnvironmentName);

            loggerFactory.AddConsole();
            loggerFactory.AddDebug();

            // use the JWT auth
            app.UseAuthentication();

            // create Soap 1.2 Binding for XCPD Service
            TextMessageEncodingBindingElement textBindingElement = new TextMessageEncodingBindingElement(MessageVersion.Soap12WSAddressing10, System.Text.Encoding.UTF8);
            HttpTransportBindingElement httpBindingElement = new HttpTransportBindingElement();
            httpBindingElement.AllowCookies = true;
            httpBindingElement.MaxBufferSize = int.MaxValue;
            httpBindingElement.MaxReceivedMessageSize = int.MaxValue;
            CustomBinding soap12Binding = new CustomBinding(textBindingElement, httpBindingElement);


            //playing around with Soap Services
            app.UseSoapEndpoint<SoapService>("/SoapService.svc", new BasicHttpBinding());
            //app.UseSoapEndpoint<XcpdService>("/XcpdService.svc", new BasicHttpBinding());
            app.UseSoapEndpoint<XcpdService>("/XcpdService.svc", soap12Binding); //, SoapSerializer.XmlSerializer);

            app.UseCors("CorsPolicy");

            app.UseSwaggerDocumentation();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
