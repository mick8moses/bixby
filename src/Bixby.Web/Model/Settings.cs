﻿namespace Bixby.Web.Model
{
    public class Settings
    {
        /// <summary>
        /// Database Connection String
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Database Name
        /// </summary>
        public string Database { get; set; }
    }
}
