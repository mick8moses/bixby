﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace Bixby.Web.Model
{
    public class WeightInfo
    {
        /// <summary>
        /// Internal Id
        /// </summary>
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; set; }

        /// <summary>
        /// Weight value
        /// REQUIRED
        /// </summary>
        public Single Weight {get; set;}

        /// <summary>
        /// DateTime Measurement was taken
        /// REQUIRED
        /// </summary>
        public DateTimeOffset MeasuredDateTime { get; set; } = DateTime.Now;
    }
}
