﻿using Bixby.Web.Infrastructure;
using Bixby.Web.Interfaces;
using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Unity.Attributes;
using System.Net.Http;
using System;

namespace Bixby.Web.Controllers
{
    /// <summary>
    /// FHIR Controller REST Web API
    /// http://hl7.org/fhir/stu3/http.html
    /// </summary>
    [Route("api/[controller]")]
    public class FhirController : Controller
    {
        // internal variable to hold db repo 
        private readonly IFhir _fhirResourceRepo;

        /// <summary>
        /// Injection Constructor For FhirController
        /// </summary>
        /// <param name="fhirResourceRepo">DB Repo</param>
        [InjectionConstructor]
        public FhirController(IFhir fhirResourceRepo)
        {
            _fhirResourceRepo = fhirResourceRepo;
        }

        /// <summary>
        /// CREATE A RESOURCE.
        /// The request body will be a FHIR Resource. If an id element is provided it will be ignored.
        /// If the request body includes meta, the versionId and lastUpdated values will be ignored.
        /// id, meta.versionId, and meta.lastUpdated will be assigned by ingesting server.
        /// </summary>
        /// <param name="resourceType">Resource Type</param>
        /// <param name="fhirResource">FHIR Resource to be created</param>
        /// <returns>
        /// 201. Success - Created HTTP status code and return Location header with new Logical Id & Version Id of the created FHIR resource.
        /// 400. Bad Request - resource could not be parsed or failed basic FHIR validation rules.
        /// 404. Not Found - FHIR resource type not supported.
        /// 422. Unprocessable Entity - FHIR resource violated applicable FHIR profiles &/or business rules
        /// </returns>
        [NoCache]
        [HttpPost, Route("{resourceType}", Order = 0)]
        public async Task<IActionResult> Post(string resourceType, [FromBody, Required] Resource fhirResource)
        {
            //try // this fails on unit tests
            //{
            //    System.Diagnostics.Trace.WriteLine("POST: " + resourceType + " : " + Utility.ToUri(this.Request));
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //var baseUri = this.CalculateBaseURI("{ResourceName}");

            // If no FHIR Resource is provided in the body of the post or it can't be mapped to a valid FHIR resource
            if (fhirResource == null)
            {
                var oo = new OperationOutcome()
                {
                    Text = Utility.CreateNarative("Validation Error"),
                };
                oo.Issue = new List<OperationOutcome.IssueComponent>
                {
                    new OperationOutcome.IssueComponent()
                    {
                        Details = new CodeableConcept(null, null, "Missing " + resourceType + " resource POST"),
                        Severity = OperationOutcome.IssueSeverity.Fatal
                    }
                };
                // throw a Bad Request status with the Note
                return (IActionResult)StatusCode(StatusCodes.Status400BadRequest, oo);
            }
            else
                return (IActionResult)StatusCode(StatusCodes.Status422UnprocessableEntity);

           // IFhirResourceServiceSTU3 model = GetResourceModel(ResourceName, GetInputs(buri));
        }

        ///// <summary>
        ///// Private Async Method to Add a FHIR Resource
        ///// </summary>
        ///// <returns>added Resource</returns>
        //private async Task<Resource> AddResourceInternal(Resource resource)
        //{
        //     return await _resourceRepo.Add(resource);
        //}

    }
}
