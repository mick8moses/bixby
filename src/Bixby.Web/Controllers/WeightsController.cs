﻿using Bixby.Web.Infrastructure;
using Bixby.Web.Interfaces;
using Bixby.Web.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Unity.Attributes;

namespace Bixby.Web.Controllers
{
    /// <summary>
    /// Weight Controller REST Web API
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class WeightsController : Controller
    {
        // internal variable to hold db repo 
        private readonly IWeight _weightRepo;

        /// <summary>
        /// Injection Constructor For WeightsController
        /// </summary>
        /// <param name="weightRepo">DB Repo</param>
        [InjectionConstructor]
        public WeightsController(IWeight weightRepo)
        {
            _weightRepo = weightRepo;
        }

        /// <summary>
        /// Add a New Weight Measurement via the Controller
        /// </summary>
        /// <param name="weight">weight measurement</param>
        /// <returns>returns weight mesurement added</returns>        
        /// <remarks>POST api/weights</remarks>
        [NoCache]
        [HttpPost, Authorize]
        public Task<WeightInfo> Post([FromBody, Required] WeightInfo weight)
        {
            // get the user
            try
            {
                ClaimsPrincipal currentUser = HttpContext.User;
            }
            catch
            { }

            return AddWeightInternal(new WeightInfo() { Weight = weight.Weight});
        }

        /// <summary>
        /// Get All Weight Measurements via the Controller
        /// </summary>
        /// <returns>all weight measurements</returns>
        /// <remarks>GET api/weights</remarks>
        [NoCache]
        [HttpGet, Authorize]
        public Task<IEnumerable<WeightInfo>> Get()
        {
            // get the user
            try
            {
                ClaimsPrincipal currentUser = HttpContext.User;
            }
            catch
            { }

            return GetWeightsInternal();
        }

        /// <summary>
        /// Private Async Method to Get All Weight Measurements
        /// </summary>
        /// <returns>all weight measurements</returns>
        private async Task<IEnumerable<WeightInfo>> GetWeightsInternal()
        {
            return await _weightRepo.GetAllWeightMeasurements();
        }

        /// <summary>
        /// Private Async Method to Add a Weight Measurement
        /// </summary>
        /// <returns>added weight measurement id</returns>
        private async Task<WeightInfo> AddWeightInternal(WeightInfo weight)
        {
            if (weight.Weight > 0)
                return await _weightRepo.Add(weight);
            else
                return null;
        }

    }
}

