﻿using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bixby.Web.Interfaces
{
    /// <summary>
    /// Contract for FHIR Resources
    /// </summary>
    public interface IFhir
    {
        /// <summary>
        /// Add a FHIR Resource
        /// </summary>
        /// <param name="resourceType">Resource Type</param>
        /// <param name="fhirResource">FHIR Resource to be created</param>
        Task<IActionResult> Post(string resourceType, Resource fhirResource);
    }
}
