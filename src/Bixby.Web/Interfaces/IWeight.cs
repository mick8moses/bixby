﻿using Bixby.Web.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bixby.Web.Interfaces
{
    /// <summary>
    /// Contract for Weight Measurements
    /// </summary>
    public interface IWeight
    {
        /// <summary>
        /// Add a Weight Measurement
        /// </summary>
        /// <param name="weight">weight measurement to add - async</param>
        /// <returns>weight measurement - async</returns>
        Task<WeightInfo> Add(WeightInfo weight);

        /// <summary>
        /// Get All the Weight Measurements
        /// </summary>
        /// <returns>enumerable of weight measurements - async</returns>
        Task<IEnumerable<WeightInfo>> GetAllWeightMeasurements();
    }
}
