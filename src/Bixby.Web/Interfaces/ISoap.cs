﻿using System.ServiceModel;

namespace Bixby.Web.Interfaces
{
    [ServiceContract]
    public interface ISoap
    {
        [OperationContract]
        string Ping(string s);
    }
}
