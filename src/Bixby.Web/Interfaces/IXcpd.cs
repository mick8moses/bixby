﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Bixby.Web.Interfaces
{
    /// <summary>
    /// Contract for XCPD (ITI-55) 
    /// IHE Standard
    /// </summary>
    [ServiceContract(Namespace = "urn:ihe:iti:xcpd:2009")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
    public interface IXcpd
    {
        /// <summary>
        /// ITI-55 https://wiki.ihe.net/index.php/Cross-Community_Patient_Discovery
        /// </summary>
        /// <param name="request">XCPD Message Request</param>
        /// <returns>XCPD Message Response</returns>
        [OperationContract(Action = "urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery", ReplyAction = "urn:hl7-org:v3:PRPA_IN201306UV02:CrossGatewayPatientDiscovery")]
        Message QueryByDemographics(Message request);
    }
}

