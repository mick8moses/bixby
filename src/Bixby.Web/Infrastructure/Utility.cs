﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Bixby.Web.Infrastructure
{
    public static class Utility
    {
        public static Hl7.Fhir.Model.Narrative CreateNarative(string div)
        {
            var n = new Hl7.Fhir.Model.Narrative();
            if (!string.IsNullOrEmpty(div) && !div.StartsWith("<div", StringComparison.CurrentCultureIgnoreCase))
                n.Div = String.Format("<div xmlns=\"http://www.w3.org/1999/xhtml\">{0}</div>", div);
            else
                n.Div = div;
            return n;
        }
        //public static Uri ToUri(HttpRequest request)
        //{
        //    var hostComponents = request.Host.ToUriComponent().Split(':');

        //    var builder = new UriBuilder
        //    {
        //        Scheme = request.Scheme,
        //        Host = hostComponents[0],
        //        Path = request.Path,
        //        Query = request.QueryString.ToUriComponent()
        //    };

        //    if (hostComponents.Length == 2)
        //    {
        //        builder.Port = Convert.ToInt32(hostComponents[1]);
        //    }

        //    return builder.Uri;
        //}

        //#region << Controller Extensions >>
        //public static string CalculateBaseURI(this Controller me, string resourceName)
        //{
        //    Uri ri = me.ControllerContext.Request.RequestUri;
        //    if (resourceName == "metadata" || resourceName == "${operation}")
        //    {
        //        return String.Format("{0}://{1}{2}{3}{4}/",
        //            ri.Scheme,
        //            ri.Host,
        //            ri.IsDefaultPort ? "" : ":" + ri.Port.ToString(),
        //            me.ControllerContext.RequestContext.VirtualPathRoot.TrimEnd('/') + '/',
        //            me.ControllerContext.RouteData.Route.RouteTemplate.Replace("/metadata", "").Replace("/${operation}", ""));
        //    }
        //    if (me.ControllerContext.RouteData.Route.RouteTemplate.Contains("{ResourceName}"))
        //        resourceName = "{ResourceName}";
        //    string baseUri = String.Format("{0}://{1}{2}{3}{4}",
        //        ri.Scheme,
        //        ri.Host,
        //        ri.IsDefaultPort ? "" : ":" + ri.Port.ToString(),
        //        me.ControllerContext.RequestContext.VirtualPathRoot.TrimEnd('/') + '/',
        //        me.ControllerContext.RouteData.Route.RouteTemplate.Substring(0, me.ControllerContext.RouteData.Route.RouteTemplate.LastIndexOf(resourceName)));
        //    return baseUri;
        //}

        //#endregion

    }
}
