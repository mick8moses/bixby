﻿using Bixby.Web.Model;
using Hl7.Fhir.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Bixby.Web.Data
{
    /// <summary>
    /// DB Context for FHIR Resources
    /// </summary>
    public class FhirContext
    {
        // internal variable for Mongo DB
        private readonly IMongoDatabase _database = null;

        /// <summary>
        /// Contructor for DB Context for FHIR Resources
        /// </summary>
        /// <param name="settings">database settings</param>
        public FhirContext(IOptions<Settings> settings)
        {
            // create a client to access the Mongo DB
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        /// <summary>
        /// Get the Fhir Resources DB
        /// </summary>
        public IMongoCollection<Resource> FhirResources
        {
            get
            {
                return _database.GetCollection<Resource>("FhirResource");
            }
        }
    }
}
