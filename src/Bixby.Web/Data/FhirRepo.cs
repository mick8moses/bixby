﻿using Bixby.Web.Interfaces;
using Bixby.Web.Model;
using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Bixby.Web.Data
{
    /// <summary>
    /// Data Access Class for FHIR Resources
    /// </summary>
    public class FhirRepo : Controller, IFhir
    {
        // internal variable to hold the DB context
        private readonly FhirContext _context = null;

        /// <summary>
        /// Constructor for FHIR Resources Data Access Class
        /// </summary>
        /// <param name="settings">database settings</param>
        public FhirRepo(IOptions<Settings> settings)
        {
            // set the DB context
            _context = new FhirContext(settings);
        }

        /// <summary>
        /// Add a FHIR Resource
        /// </summary>
        /// <param name="resourceType">Resource Type</param>
        /// <param name="fhirResource">FHIR Resource to be created</param>
        /// <returns>
        /// 201. Success - Created HTTP status code and return Location header with new Logical Id & Version Id of the created FHIR resource.
        /// 400. Bad Request - resource could not be parsed or failed basic FHIR validation rules.
        /// 404. Not Found - FHIR resource type not supported.
        /// 422. Unprocessable Entity - FHIR resource violated applicable FHIR profiles &/or business rules
        /// </returns>
        public async Task<IActionResult> Post(string resourceType, Resource fhirResource)
        {
            try
            {
                await _context.FhirResources.InsertOneAsync(fhirResource);
                return StatusCode(StatusCodes.Status201Created, fhirResource);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}
