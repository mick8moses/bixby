﻿using Bixby.Web.Interfaces;
using Bixby.Web.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bixby.Web.Data
{
    /// <summary>
    /// Data Access Class for Weight Measurements
    /// </summary>
    public class WeightRepo : IWeight
    {
        // internal variable to hold the DB context
        private readonly WeightContext _context = null;

        /// <summary>
        /// Constructor for Weight Measurement Data Access Class
        /// </summary>
        /// <param name="settings">database settings</param>
        public WeightRepo(IOptions<Settings> settings)
        {
            // set the DB context
            _context = new WeightContext(settings);
        }

        /// <summary>
        /// Add a Weight Measurement
        /// </summary>
        /// <param name="weight">weight measurement to add - async</param>
        public async Task<WeightInfo> Add(WeightInfo weight)
        {
            try
            {
                await _context.Weights.InsertOneAsync(weight);
                return weight;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        /// <summary>
        /// Get All the Weight Measurements
        /// </summary>
        /// <returns>enumerable of weight measurements - async</returns>
        public async Task<IEnumerable<WeightInfo>> GetAllWeightMeasurements()
        {
            try
            {
                return await _context.Weights.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}
