﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Bixby.Web.Model;

namespace Bixby.Web.Data
{
    /// <summary>
    /// DB Context for Weight Measurements
    /// </summary>
    public class WeightContext
    {
        // internal variable for Mongo DB
        private readonly IMongoDatabase _database = null;

        /// <summary>
        /// Contructor for DB Context for Weight Measurements
        /// </summary>
        /// <param name="settings">database settings</param>
        public WeightContext(IOptions<Settings> settings)
        {
            // create a client to access the Mongo DB
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        /// <summary>
        /// Get the Weight Measurement DB
        /// </summary>
        public IMongoCollection<WeightInfo> Weights
        {
            get
            {
                return _database.GetCollection<WeightInfo>("Weight");
            }
        }
    }
}
