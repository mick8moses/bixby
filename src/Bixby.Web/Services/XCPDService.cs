﻿using Bixby.Web.Interfaces;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;

namespace Bixby.Web.Services
{

    //public class XcpdService : IXcpd
    //{
    //    public Message QueryByDemographics(ref Message requestMsg)
    //    {
    //        // create a buffered copy of the message
    //        MessageBuffer requestMsgBuffer = requestMsg.CreateBufferedCopy(Int32.MaxValue);


    //        //let's try and call an xcpd endpoint
    //        BasicHttpBinding binding = new BasicHttpBinding();
    //        EndpointAddress endpoint = new EndpointAddress(new Uri("http://zgen-qa-app:18300/xcpd"));

    //        Message result;
    //        using (ChannelFactory<IXcpd> channelFactory = new ChannelFactory<IXcpd>(binding, endpoint))
    //        {
    //            IXcpd instance = channelFactory.CreateChannel();
    //            result = instance.QueryByDemographics();
    //        }
    //        return result;
    //        //throw new System.NotImplementedException();
    //        //return requestMsgBuffer.CreateMessage();
    //    }

    //    public Message QueryByDemographics()
    //    { return null; }
    //}


    /// <summary>
    /// ITI-55 https://wiki.ihe.net/index.php/Cross-Community_Patient_Discovery
    /// http://localhost/XcpdService.svc?wsdl
    /// </summary>
    /// <param name="request">XCPD Message Request</param>
    /// <returns>XCPD Message Response</returns>
    public class XcpdService : IXcpd
    {
        public Message QueryByDemographics(Message request)
        {
            // let's try and call an xcpd endpoint
            BasicHttpBinding binding = new BasicHttpBinding();
            EndpointAddress endpoint = new EndpointAddress(new Uri("http://zgen-qa-app:18300/xcpd"));

            Message result;
            using (ChannelFactory<IXcpd> channelFactory = new ChannelFactory<IXcpd>(binding, endpoint))
            {
                IXcpd instance = channelFactory.CreateChannel();
                result = instance.QueryByDemographics(request);
            }
            return result;
            //throw new System.NotImplementedException();
        }
    }

}


