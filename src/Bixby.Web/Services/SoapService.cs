﻿using Bixby.Web.Interfaces;
using System.Linq;

namespace Bixby.Web.Services
{
    /// <summary>
    /// Practice Soap Service
    /// http://localhost/SoapService.svc?wsdl
    /// </summary>
    public class SoapService : ISoap
    {
        public string Ping(string s)
        {
            return string.Join(string.Empty, s.Reverse());
        }
    }
}

